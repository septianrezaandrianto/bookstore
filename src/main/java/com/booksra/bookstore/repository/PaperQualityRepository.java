package com.booksra.bookstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.booksra.bookstore.model.PaperQuality;

@Repository
public interface PaperQualityRepository extends JpaRepository<PaperQuality, Long>{

}
