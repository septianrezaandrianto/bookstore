package com.booksra.bookstore.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "publishers")
@JsonIgnoreProperties({"books"})
public class Publisher {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long publisherId;
	
	@Column(name ="companyName", nullable = false)
	private String companyName;
	
	@Column(name="country")
	private String country;
	
	@ManyToOne
	@JoinColumn(name = "paper")
	private PaperQuality paper;

	@OneToMany
	(mappedBy = "publisher", cascade = CascadeType.ALL)
	private Set<Book> books;

	public Publisher() {
		super();
	}

	

	public Publisher(Long publisherId, String companyName, String country, PaperQuality paper) {
		super();
		this.publisherId = publisherId;
		this.companyName = companyName;
		this.country = country;
		this.paper = paper;

	}
	
	public Long getPublisherId() {
		return publisherId;
	}

	public void setPublisherId(Long publisherId) {
		this.publisherId = publisherId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}


	public PaperQuality getPaper() {
		return paper;
	}


	public void setPaper(PaperQuality paper) {
		this.paper = paper;
	}


	public Set<Book> getBooks() {
		return books;
	}


	public void setBooks(Set<Book> books) {
		this.books = books;
	}


	
	

	
}
