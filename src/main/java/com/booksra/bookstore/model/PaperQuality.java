package com.booksra.bookstore.model;


import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "paper_qualities")
@JsonIgnoreProperties({"publishers"})
public class PaperQuality {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long paperId;
	
	@Column(name ="qualityName", nullable = false)
	private String qualityName;
	
	@Column(name ="paperPrice", nullable = false)
	private double paperPrice;

	@OneToMany
	(mappedBy ="paper",cascade = CascadeType.ALL)
	private Set<Publisher> publishers;
	
	
	
	public PaperQuality() {
		super();
	}
	
	public PaperQuality(Long paperId, String qualityName, double paperPrice, Publisher...publishers) {
		super();
		this.paperId = paperId;
		this.qualityName = qualityName;
		this.paperPrice = paperPrice;
		this.publishers = Stream.of(publishers).collect(Collectors.toSet());
        this.publishers.forEach(x -> x.setPaper(this));
	}


	public Long getPaperId() {
		return paperId;
	}

	public void setPaperId(Long paperId) {
		this.paperId = paperId;
	}

	public Set<Publisher> getPublishers() {
		return publishers;
	}

	public void setPublishers(Set<Publisher> publishers) {
		this.publishers = publishers;
	}

	public String getQualityName() {
		return qualityName;
	}

	public void setQualityName(String qualityName) {
		this.qualityName = qualityName;
	}

	public double getPaperPrice() {
		return paperPrice;
	}

	public void setPaperPrice(double paperPrice) {
		this.paperPrice = paperPrice;
	}
	
	
}
