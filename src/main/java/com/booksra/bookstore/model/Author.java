package com.booksra.bookstore.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "authors")
@JsonIgnoreProperties({"books"})
public class Author {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long authorId;
	
	@Column(name="firstName", nullable = false)
	private String firstName;
	
	@Column(name ="lastName", nullable = false)
	private String lastName;
	
	@Column(name ="gender", nullable = false)
	private String gender;
	
	@Column(name ="age")
	private int Age;
	
	@Column(name ="country")
	private String country;
	
	@Column(name ="rating", nullable = false)
	private String rating;

	@OneToMany
	(mappedBy = "author", cascade = CascadeType.ALL)
	private Set<Book> books;
	
	public Author() {
		super();
	}

	
	public Author(Long authorId, String firstName, String lastName, String gender, int age, String country,
			String rating) {
		super();
		this.authorId = authorId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		Age = age;
		this.country = country;
		this.rating = rating;
	}

	
	public Long getAuthorId() {
		return authorId;
	}


	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}


	public Set<Book> getBooks() {
		return books;
	}


	public void setBooks(Set<Book> books) {
		this.books = books;
	}


	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return Age;
	}

	public void setAge(int age) {
		Age = age;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}
	
	
}
