package com.booksra.bookstore.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.booksra.bookstore.model.Author;
import com.booksra.bookstore.repository.AuthorRepository;

@RestController
@RequestMapping("/api")
public class AuthorController {

	@Autowired
	AuthorRepository authorRepository;
	
//	Melihat list Authors
	@GetMapping("authors/all")
	public HashMap<String, Object> getAllAuthors() {
		HashMap<String, Object> hasAuthors = new HashMap<String, Object>();
		
		hasAuthors.put("Message : ", "All Data");
		hasAuthors.put("Data : ", authorRepository.findAll());
		
	return hasAuthors;
	}
	
//	Melihat sebuah author
	@GetMapping("authors/{id}")
	public HashMap<String, Object> getAuthorById(@PathVariable(value="id") Long authorId) {
		
		HashMap<String, Object> hasAuthors = new HashMap<String, Object>();
		
		Author aut = authorRepository.findById(authorId)
				.orElse(null);
		
		hasAuthors.put("Data : " , aut);
		
		return  hasAuthors;
	}
	
//	Menambahkan sebuah authors baru
	@PostMapping("/authors/add")
	public HashMap<String, Object> createAuthor(@Valid @RequestBody Author author) {
		
		HashMap<String, Object> hasAuthors = new HashMap<String, Object>();
		
		hasAuthors.put("Message : ", "Create Success");
		hasAuthors.put("Data : ", authorRepository.save(author));
	
	return hasAuthors;
	}
	
//	Menambahkan banyak authors baru
	@PostMapping("/authors/add/more")
	public HashMap<String, Object> createMoreAuthors(@Valid @RequestBody Author...authors) {
		
		HashMap<String, Object> mapAuthors = new HashMap<String, Object>();
		ArrayList<Author> listAuthor = new ArrayList<Author>();
		
		for (Author au : authors) {
			authorRepository.save(au);
			listAuthor.add(au);
		}
		
		mapAuthors.put("Message :" , "Create Success");
		mapAuthors.put("Total Insert : ", listAuthor.size());
		mapAuthors.put("Data insert :" , listAuthor);
		
	return mapAuthors;
	}
	
	
//	Mengupdate data authors
	@PutMapping("/authors/update/{id}")
	public HashMap<String, Object> updateAuthors(@PathVariable(value="id") Long authorId, @Valid @RequestBody Author authorDetails) {
		
		HashMap<String, Object> hashAuthors = new HashMap<String, Object>();
		
		Author au = authorRepository.findById(authorId)
 				.orElse(null);
		
		authorDetails.setAuthorId(au.getAuthorId());
		
		if (authorDetails.getAge() == 0) {
			authorDetails.setAge(au.getAge());
		}	
		
		if (authorDetails.getCountry() == null) {
			authorDetails.setCountry(au.getCountry());
		}
		
		if (authorDetails.getFirstName() == null) {
			authorDetails.setFirstName(au.getFirstName());
		}
		
		if (authorDetails.getLastName() == null) {
			authorDetails.setLastName(au.getLastName());
		}
		
		if (authorDetails.getGender() == null) {
			authorDetails.setGender(au.getGender());
		}
		
		if (authorDetails.getRating() == null) {
			authorDetails.setRating(au.getGender());
		}
		
		authorRepository.save(authorDetails);
		
		hashAuthors.put("Message :" , "Update Success");
		hashAuthors.put("Data : ", authorDetails);
		
	return hashAuthors;
	}
	
	
//	Menghapus authors
	@DeleteMapping("/authors/delete/{id}")
	public HashMap<String, Object> deleteAuthorById(@PathVariable(value = "id") Long authorId) {
		
		HashMap<String, Object> mapAuthor = new HashMap<String, Object>();
		
		Author aut = authorRepository.findById(authorId)
	     				.orElse(null);
		
		authorRepository.delete(aut);
		
		mapAuthor.put("Message : " , "Delete Success");
	return mapAuthor;
	   
	 }
}
