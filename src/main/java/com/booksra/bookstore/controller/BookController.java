package com.booksra.bookstore.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.booksra.bookstore.model.Book;
import com.booksra.bookstore.repository.BookRepository;

@RestController
@RequestMapping("/api")
public class BookController {

	@Autowired
	private BookRepository bookRepository;
	
	
//	Melihat list Books
	@GetMapping("books/all")
	public HashMap<String, Object> getAllBooks() {
		
		HashMap<String, Object> hasBooks = new HashMap<String, Object>();
		
		hasBooks.put("Message : ", "All Data");
		hasBooks.put("Data : ", bookRepository.findAll());
	return hasBooks;
	}	
	
//	Melihat sebuah books
	@GetMapping("books/{id}")
	public HashMap<String, Object> getBookById(@PathVariable(value="id") Long bookId) {
		
		HashMap<String, Object> hasBooks = new HashMap<String, Object>();
		
		Book book = bookRepository.findById(bookId)
				.orElse(null);
		
		hasBooks.put("Data : " , book);
		
		return  hasBooks;
	}
	
//	Menambahkan sebuah books baru
	@PostMapping("/books/add")
	public HashMap<String, Object> createBook(@Valid @RequestBody Book book) {
		
		HashMap<String, Object> hasBooks = new HashMap<String, Object>();
		
		hasBooks.put("Message : " , "Create Success");
		hasBooks.put("Data : ", bookRepository.save(book));
	return hasBooks;
	}
	
//	Menambahkan banyak books baru
	@PostMapping("/books/add/more")
	public HashMap<String, Object> createMoreBooks(@Valid @RequestBody Book...books) {
		
		HashMap<String, Object> mapBooks = new HashMap<String, Object>();
		ArrayList<Book> listBook = new ArrayList<Book>();
		
		for (Book b : books) {
			bookRepository.save(b);
			listBook.add(b);
		}
		
		mapBooks.put("Message :" , "Create Success");
		mapBooks.put("Total Insert : ", listBook.size());
		mapBooks.put("Data insert :" , listBook);
		
		return mapBooks;
	}
	
	
//	Mengupdate data books
	@PutMapping("/books/update/{id}")
	public HashMap<String, Object> updateBook(@PathVariable(value="id") Long bookId, @Valid @RequestBody Book bookDetails) {
		
		HashMap<String, Object> hashBooks = new HashMap<String, Object>();
		
		Book book = bookRepository.findById(bookId)
 				.orElse(null);
		
		bookDetails.setBookId(book.getBookId());
		
		if (bookDetails.getPrice() == 0) {
			bookDetails.setPrice(book.getPrice());
		}	
		
		if (bookDetails.getTitle() == null) {
			bookDetails.setTitle(book.getTitle());
		}
		
		if (bookDetails.getReleaseDate() == null) {
			bookDetails.setReleaseDate(book.getReleaseDate());
		}
		
		
		bookRepository.save(bookDetails);
		
		hashBooks.put("Message :" , "Update Success");
		hashBooks.put("Data : ", bookDetails);
		
		return hashBooks;
	}
	
	
//	Menghapus books
	@DeleteMapping("/books/delete/{id}")
	public HashMap<String, Object> deleteBookById(@PathVariable(value = "id") Long bookId) {
		
		HashMap<String, Object> mapBooks = new HashMap<String, Object>();
		
		Book book = bookRepository.findById(bookId)
	     				.orElse(null);
		
		bookRepository.delete(book);
		
		mapBooks.put("Message : " , "Delete Success");
		return mapBooks;  
	 }
	
}
