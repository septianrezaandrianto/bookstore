package com.booksra.bookstore.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.booksra.bookstore.model.PaperQuality;
import com.booksra.bookstore.repository.PaperQualityRepository;

@RestController
@RequestMapping("/api")
public class PaperQualityController {

	@Autowired
	private PaperQualityRepository paperQualityRepository;

//	Melihat list Paper Quality
	@GetMapping("papers/all")
	public HashMap<String, Object> getAllPapers() {
		
		HashMap<String, Object> hasPapers = new HashMap<String, Object>();
		
		hasPapers.put("Message :", "All Data");
		hasPapers.put("Data" , paperQualityRepository.findAll());
		
		return hasPapers;
	}
	
	
//	Melihat sebuah Paper Quality
	@GetMapping("papers/{id}")
	public HashMap<String, Object> getPaperById(@PathVariable(value="id") Long paperId) {
		
		HashMap<String, Object> hasPaper = new HashMap<String, Object>();
		
		PaperQuality pq = paperQualityRepository.findById(paperId)
				.orElse(null);
		
		hasPaper.put("Data : " , pq);
		
		return  hasPaper;
	}
	
//	Menambahkan sebuah Paper Quality baru
	@PostMapping("/papers/add")
	public HashMap<String, Object> createPaper(@Valid @RequestBody PaperQuality paperQuality) {
		HashMap<String, Object> hasPapers = new HashMap<String, Object>();
		
		hasPapers.put("Message :", "Create Success");
		hasPapers.put("Data : ", paperQualityRepository.save(paperQuality));
		
		return hasPapers;
	}
	
	
//	Menambahkan banyak Paper Quality baru
	@PostMapping("/papers/add/more")
	public HashMap<String, Object> createMorePapers(@Valid @RequestBody PaperQuality...paperQualities) {
		
		HashMap<String, Object> mapPaper = new HashMap<String, Object>();
		ArrayList<PaperQuality> listPaper = new ArrayList<PaperQuality>();
		
		for (PaperQuality pq : paperQualities) {
			paperQualityRepository.save(pq);
			listPaper.add(pq);
		}
		
		mapPaper.put("Message :" , "Create Success");
		mapPaper.put("Total Insert : ", listPaper.size());
		mapPaper.put("Data insert :" , listPaper);
		
		return mapPaper;
	}
	
//	Mengupdate data Paper Quality
	@PutMapping("/papers/update/{id}")
	public HashMap<String, Object> updatePaper(@PathVariable(value="id") Long paperId, @Valid @RequestBody PaperQuality paperQualityDetails) {
		
		HashMap<String, Object> hashPaper = new HashMap<String, Object>();
		
		PaperQuality pq = paperQualityRepository.findById(paperId)
 				.orElse(null);
		
		paperQualityDetails.setPaperId(pq.getPaperId());
		
		if(paperQualityDetails.getPaperPrice() == 0) {
			paperQualityDetails.setPaperPrice(pq.getPaperPrice());
		}	
		
		paperQualityRepository.save(paperQualityDetails);
		
		hashPaper.put("Message :" , "Update Success");
		hashPaper.put("Data : ", paperQualityDetails);
		
		return hashPaper;
	}
	
	
//	Menghapus papaer Quality
	@DeleteMapping("/papers/delete/{id}")
	public HashMap<String, Object> deletePaperById(@PathVariable(value = "id") Long paperId) {
		
		HashMap<String, Object> mapPaper = new HashMap<String, Object>();
		
		PaperQuality pq = paperQualityRepository.findById(paperId)
	     				.orElse(null);
		
		paperQualityRepository.delete(pq);
		
		mapPaper.put("Message : " , "Delete Success");
		return mapPaper;	   
	 }
	
}
