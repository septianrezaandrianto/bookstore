package com.booksra.bookstore.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.booksra.bookstore.model.Publisher;
import com.booksra.bookstore.repository.PublisherRepository;

@RestController
@RequestMapping("/api")
public class PublisherController {

	@Autowired
	PublisherRepository publisherRepository;
	
//	Melihat list Publisher
	@GetMapping("publishers/all")
	public HashMap<String, Object> getAllPublishers() {
		HashMap<String, Object> hasPublisher = new HashMap<String, Object>();
		
		hasPublisher.put("Message :" , "All Data");
		hasPublisher.put("Data : ", publisherRepository.findAll());
		
	return hasPublisher;
	}
	
//	Melihat sebuah Publisher
	@GetMapping("publishers/{id}")
	public HashMap<String, Object> getPublisherById(@PathVariable(value="id") Long publisherId) {
		
		HashMap<String, Object> hasPublisher = new HashMap<String, Object>();
		
		Publisher pub = publisherRepository.findById(publisherId)
				.orElse(null);
		
		hasPublisher.put("Data : " , pub);
		
	return  hasPublisher;
	}
	
	
//	Menambahkan sebuah Publisher baru
	@PostMapping("/publishers/add")
	public HashMap<String, Object> createPublisher(@Valid @RequestBody Publisher publisher ) {
		HashMap<String, Object> hasPublisher = new HashMap<String, Object>();
		
		hasPublisher.put("Message : ", "Create Success");
		hasPublisher.put("Data : " ,publisherRepository.save(publisher));
		
	return hasPublisher; 
	}
	
//	Menambahkan banyak Publisher baru
	@PostMapping("/publishers/add/more")
	public HashMap<String, Object> createMorePublishers(@Valid @RequestBody Publisher...publishers) {
		
		HashMap<String, Object> hasPublisher = new HashMap<String, Object>();
		ArrayList<Publisher> listPublisher = new ArrayList<Publisher>();
		
		for (Publisher pub : publishers) {
			publisherRepository.save(pub);
			listPublisher.add(pub);
		}
		
		hasPublisher.put("Message :" , "Create Success");
		hasPublisher.put("Total Insert : ", listPublisher.size());
		hasPublisher.put("Data insert :" , listPublisher);
		
		return hasPublisher;
	}
	
//	Mengupdate data publishers
	@PutMapping("/publishers/update/{id}")
	public HashMap<String, Object> updatePublisher(@PathVariable(value="id") Long publisherId, @Valid @RequestBody Publisher publisherDetails) {
		
		HashMap<String, Object> hashPublisher = new HashMap<String, Object>();
		
		Publisher pub = publisherRepository.findById(publisherId)
				.orElse(null);
		publisherDetails.setPublisherId(pub.getPublisherId());
		
		if(publisherDetails.getCompanyName() == null) {
			publisherDetails.setCompanyName(pub.getCompanyName());
		}
		
		if(publisherDetails.getCountry()==null) {
			publisherDetails.setCountry(pub.getCountry());
		}
		
		publisherRepository.save(publisherDetails);
		
		hashPublisher.put("Message :" , "Update Success");
		hashPublisher.put("Data : ", publisherDetails);
		
		return hashPublisher;
	}
	
//	Menghapus publishers Quality
	@DeleteMapping("/publishers/delete/{id}")
	public HashMap<String, Object> deletePublisherById(@PathVariable(value = "id") Long publisherId) {
		
		HashMap<String, Object> hasPublisher = new HashMap<String, Object>();
		
		Publisher pub = publisherRepository.findById(publisherId)
	     				.orElse(null);
		
		publisherRepository.delete(pub);
		
		hasPublisher.put("Message : " , "Delete Success");
		
		return hasPublisher;   
	 }	
	
}
